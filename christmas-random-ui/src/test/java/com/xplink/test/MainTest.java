package com.xplink.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class MainTest {

	private static Logger logger = LogManager.getRootLogger();
	
	@BeforeClass
	public static void onBeforeClass() {
		
	}
	
	@AfterClass
	public static void onAfterClass() {
		
	}
	
	@Test
	public void test() {
		logger.debug("test");
	}
}
