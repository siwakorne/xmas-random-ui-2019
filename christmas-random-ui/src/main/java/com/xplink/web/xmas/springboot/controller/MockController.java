package com.xplink.web.xmas.springboot.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;


@RestController
public class MockController {
	
	Gson gson = new Gson();
	
	public static void mainx(String...args) {//Fri, 12 Jun 2017 13:53:37
		SimpleDateFormat sf = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss");
		Random rnd = new Random();
		Date d = new Date(System.currentTimeMillis() + (int) (rnd.nextFloat() * 36) * 5);
		System.out.println(sf.format(d));
		new MockController().getSaltString();
	}	
	
	private String getDatetime() {
		SimpleDateFormat sf = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss");
		Random rnd = new Random();
		long time = System.currentTimeMillis() + (int) (rnd.nextFloat() * 36) * 100000000;
		Date d = new Date(time);
		return sf.format(d);
	}
	
	private BasicNameValuePair setEntity(String key, String value) {
		return new BasicNameValuePair(key, value);
	}
	
	@RequestMapping(value="/endpoint", method=RequestMethod.POST)
	public String endpoint(HttpServletRequest request) {
//		String result = request.getParameter("test");
		Map<String, String> values = new HashMap<>();
		values.put("success", "true");
		values.put("challenge_ts", "2017-11-28T11:12:27+0700");
		values.put("hostname", "http://http://35.185.176.21:8089");
		values.put("error-codes", "test error");
		
		return gson.toJson(values);
	}
	
	@RequestMapping(value="/test-thread", method=RequestMethod.GET)
	public void testThread(HttpServletResponse response) throws IOException {
		response.getWriter().write("test..write with delay...2 seconds");
		response.setHeader("Refresh", "5;url=http://google.com");
	}
	
	@ResponseBody
	@RequestMapping(value="/test", method=RequestMethod.GET, produces="application/json")
	public Map<String, Object> test(HttpServletRequest request, HttpServletResponse response) throws ClientProtocolException, IOException {
		
		
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost("https://www.google.com/recaptcha/api/siteverify");
		List<NameValuePair> values = new ArrayList<NameValuePair>();
		
		values.add(setEntity("secret", "6LdOtjoUAAAAAK_JsipJJaeu4hH5e1ZEGMXbQocf"));
		values.add(setEntity("response", request.getParameter("g-recaptcha-response")));
		values.add(setEntity("remoteip", request.getRemoteHost()));
		
		post.setEntity(new UrlEncodedFormEntity(values));

		HttpResponse resp = client.execute(post);
        BufferedReader rd = new BufferedReader(new InputStreamReader(
        		resp.getEntity().getContent()));

        String line = "";
        StringBuilder result = new StringBuilder();
        while ((line = rd.readLine()) != null) {
        	result.append(line);
        }
        
        Map<String, Object> jsonMap = new HashMap<String, Object>();
        jsonMap = gson.fromJson(result.toString(), Map.class);
        return jsonMap;
	}
	
//	public static void main(String...args) {
		//yyyy-MM-dd'T'HH:mm:ssZZ
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZ");
//		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//		System.out.println(sdf.format(timestamp));
//		Map<String, String> values = new HashMap<>();
//		values.put("success", "true");
//		values.put("challenge_ts", "2017-11-28T11:12:27+0700");
//		values.put("hostname", "http://localhost:8090");
//		values.put("error-codes", "['error1':'value of error1', 'error2':'value of error2']");
//		System.out.println(values.toString());
//	}
	
//	public static void main(String...args) {
//		try
//        {
//            ObjectMapper mapper = new ObjectMapper();
//            String jsonString = "{\"name\":\"JavaInterviewPoint\", \"department\":\"blogging\"}";
//            
//            Map<String, Object> jsonMap = new HashMap<String, Object>();
//            
//            // convert JSON string to Map
//            jsonMap = mapper.readValue(jsonString,
//                    new TypeReference<Map<String, String>>(){});
//            
//            System.out.println(jsonMap);
//        }
//        catch(IOException ie)
//        {
//            ie.printStackTrace();
//        }
//	}
	
	@RequestMapping(value="/list_group", method = RequestMethod.GET, produces = "application/json")
	public Map<String, Object> listGroup(HttpServletRequest request) throws InterruptedException {
		List<Map<String, Object>> resp = new ArrayList<>();
		
		Map<String, Object> result = new HashMap<>();
		
		int dataPerPage = Integer.valueOf(request.getParameter("data_per_page"));
		int start = Integer.valueOf(request.getParameter("start"));
		int page = (start + dataPerPage) / dataPerPage;
		
		for (int i = 0; i < dataPerPage; i++) {
			int random = 1 + (int)(Math.random() * 3);
			Map<String, Object> temp = new HashMap<>();
			temp.put("id", random);
			temp.put("group_name", getSaltString());
			temp.put("created_by", getSaltString());
			temp.put("created_datetime", getDatetime());
			temp.put("members", getSaltString());
			resp.add(temp);
		}
		
		result.put("data", resp);
		result.put("page", page);
		result.put("numberOfPages", 100);

		Thread.sleep(800);
		
		return result;
	}

	private String getSaltString() {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 5) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;

	}

}
