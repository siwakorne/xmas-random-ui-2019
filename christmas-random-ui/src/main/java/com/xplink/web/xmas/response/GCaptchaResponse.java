package com.xplink.web.xmas.response;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class GCaptchaResponse implements Serializable {

	private static final long serialVersionUID = 8884526956644602391L;
	private Boolean success;
	private Timestamp challenge_ts;
	private String hostname;
	
	
	private List error_codes;

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public Timestamp getChallenge_ts() {
		return challenge_ts;
	}

	public void setChallenge_ts(Timestamp challenge_ts) {
		this.challenge_ts = challenge_ts;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public List getError_codes() {
		return error_codes;
	}

	@JsonProperty(value="error-codes")
	public void setError_codes(List error_codes) {
		this.error_codes = error_codes;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GCaptchaResponse [success=");
		builder.append(success);
		builder.append(", challenge_ts=");
		builder.append(challenge_ts);
		builder.append(", hostname=");
		builder.append(hostname);
		builder.append(", error_codes=");
		builder.append(error_codes.toString());
		builder.append("]");
		return builder.toString();
	}

}
