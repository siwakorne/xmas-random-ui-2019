package com.xplink.web.xmas.request;

import java.io.Serializable;

public class GCaptchaRequest implements Serializable {

	private static final long serialVersionUID = 2729916169271030192L;
	private String sescret;
	private String resposne;
	private String remoteip;
	
	public String getSescret() {
		return sescret;
	}
	public void setSescret(String sescret) {
		this.sescret = sescret;
	}
	public String getResposne() {
		return resposne;
	}
	public void setResposne(String resposne) {
		this.resposne = resposne;
	}
	public String getRemoteip() {
		return remoteip;
	}
	public void setRemoteip(String remoteip) {
		this.remoteip = remoteip;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GCaptchaRequest [sescret=");
		builder.append(sescret);
		builder.append(", resposne=");
		builder.append(resposne);
		builder.append(", remoteip=");
		builder.append(remoteip);
		builder.append("]");
		return builder.toString();
	}
	
}
