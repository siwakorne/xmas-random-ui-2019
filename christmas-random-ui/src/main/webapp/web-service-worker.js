(function() {
    let service = angular.module('chrisjsherm.service.web-worker', [
        'chrisjsherm.service.uuid',
    ]);

    service.factory('WebWorkerService', WebWorkerService);

    function WebWorkerService(
        $rootScope, UuidService
    ) {

        // Private properties.
        let webWorker = new Worker('web-worker.js');

        // Activation.
        activate();

        // Public interface.
        let serviceInterface = {
            calculateCompoundAnnualGrowthRate: calculateCompoundAnnualGrowthRate,
        };

        return serviceInterface;

        // Private methods.
        function activate() {
            webWorker.addEventListener(
                'message',
                onWebWorkerResponse,
                false
            );
        }

        function calculateCompoundAnnualGrowthRate(values) {
            const MESSAGE_ID = UuidService.generate();

            webWorker.postMessage(
                JSON.stringify([
                    MESSAGE_ID,
                    'calculateCompoundAnnualGrowthRate',
                    values
                ])
            );

            return MESSAGE_ID;
        }

        function onWebWorkerResponse(message) {
            let data = JSON.parse(message.data),
                messageId = data[0],
                result = data[1];

            // Call $apply around async events, including anywhere it calls 
            // broadcast/emit.
            // Docs: https://github.com/angular/angular.js/wiki/When-to-use-$scope.$apply()
            $rootScope.$apply($rootScope.$broadcast(
                messageId,
                result
            ));
        }
    }
})();