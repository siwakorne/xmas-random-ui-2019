'use strict';

// Define entry module.
var app = angular.module('ChristmasRandom', ['ngRoute', 'ngMaterial', 'ngAnimate', 'ngStorage', 'angularUUID2',
    'angular-growl', 'smart-table', 'vcRecaptcha'
]);

angular.module('ChristmasRandom')

.config(
    ['$locationProvider', '$routeProvider',
        function config($locationProvider, $routeProvider) {
            $locationProvider.hashPrefix('!');
            $routeProvider
                .when('/signin', {
                    template: '<signin></signin>'
                })
                .when('/signup', {
                    template: '<signup></signup>'
                })

            // new version

            .when('/overview', {
                    template: '<overview></overview>',
                    // resolve: {
                    // 	flag: function ($log, $location, $localStorage) {}
                    // }
                })
                .when('/event', {
                    template: '<event></event>'
                })
                .when('/settings', {
                    template: '<settings></settings>'
                })
                .when('/game', {
                    template: '<game></game>'
                })

            // end new version

            .otherwise('/signin', {
                template: '<signin></signin>'
            })
        }
    ]);

app.constant('Constant', {
    endpoint: location.protocol + "//" + location.hostname + ':8089/christmas-random-api',
    // endpoint: 'http://10.168.1.196:8089/christmas-random-api',
    default: {
        numberOfPages: 1,
    },
    reCaptchaV2: {
        siteKey: "6LdOtjoUAAAAANnS0WXy9KN8hg83z0ZcI1NYyK8T"
    },
    errorCodes: [
        'T1000', 'T1001', 'T1002', 'B1000', 'B1001', 'B1002', 'B1003',
        'B1004', 'B1005', 'B1006', 'B1007', 'B1008', 'B1009', 'B1010',
        'B1011', 'B1012', 'B1013', 'B1014', 'B1015', 'B1016', 'B1017',
        'B1018', 'B1019', 'B1020', 'B1021', 'B1022', 'B1023', 'B1024',
        'B1025', 'B1026', 'B1027'
    ]

});

app.run(function($log, $rootScope, $location, $timeout, $localStorage, $window) {
    $rootScope.user = $localStorage.user || undefined;
    // $rootScope.user = $localStorage.user;
    // $rootScope.role = $localStorage.user.role == 'ADMIN' || $localStorage.user.role == 'USER' ? 
    // 					$localStorage.user.role : 'anonymous'; 


    $timeout(function() {
        var p = $location.path();
        $(".xmas-menu").removeClass("active-menu");
        $("a[href='#!" + p + "']").addClass("active-menu");
    });

    $rootScope.$on("routePermission", function(event, data) {
        if (data.route == 'event' && $rootScope.role == 'ADMIN') $location.path('overview');
    });

    $rootScope.$on("$locationChangeStart", function(event, next, current) {
        swal.close()
        $localStorage.user = $localStorage.user || {};
        $rootScope.user = $localStorage.user
        var path = $location.path();
        var params = $location.search();
        $(".xmas-menu").removeClass("active-menu");
        $("a[href='#!" + path + "']").addClass("active-menu");

        if ($localStorage.user.username != undefined && next.indexOf('signin') > -1) {
            $location.path("overview");
        } else if ($localStorage.user.username == undefined && next.indexOf('signin') == -1 && next.indexOf('signup') == -1) {
            $location.path("signin");
        }
    });
});

app.config(['growlProvider', function(growlProvider) {
    growlProvider.globalTimeToLive(5000);
}]);