'use strict';

var app = angular.module('ChristmasRandom');

app.controller('MenuController', function ($scope, $rootScope, $localStorage, $log, $location, Util) {
    $scope.signout = function () { 
        Util.clearUser();
        $rootScope.user = $localStorage.user;
        $location.path('signin'); 
    };
});