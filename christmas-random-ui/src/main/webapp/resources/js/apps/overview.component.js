'use strict';

var app = angular.module('ChristmasRandom');

app.component(
	'overview',
	{
		templateUrl: 'pages/overview.html',
		controller: function ($scope, $rootScope, $log, Constant, Service, Util, $location, $route, $filter) {
			var firstTime = true;
			$scope.groupFilter = "all-group";
			$scope.descriptions = null;

			$scope.numberOfPages = Constant.default.numberOfPages;
			$scope.createGroupPopup = Util.createGroupPopup;
			$scope.accessGroup = function (groupName) {
				$location.path('game').search({ group_name: groupName });
			};

			$scope.requestGroupPopup = Util.requestGroupPopup;

			// $scope.findArrayObject = function (no) {
			// 	return $scope.descriptions.find((obj) => {
			// 		return obj.no == no;
			// 	});
			// };

			$scope.showDescription = function (no) {
				swal({
					type: 'info',
					title: 'Description',
					html: $scope.findArrayObject(no),
					showConfirmButton: false,
					showCancelButton: true,
					cancelButtonText: 'Closes'
				}).catch(swal.noop);
			};

			$scope.findArrayObject = function (no) {
				return $filter('filter')($scope.descriptions, {no: no}, true)[0].description;
			}

			$scope.callService = function (tableState) {
				$scope.isLoading = true;
				var start = tableState.pagination.start;
				var dataPerPage = tableState.pagination.number;
				var sortBy = Util.isPropExist(tableState.sort, 'reverse') < 0 ? '' : Util.convertSortName(tableState.sort.reverse);
				var sortField = Util.isPropExist(tableState.sort, 'predicate') < 0 ? '' : tableState.sort.predicate;
				var search = Util.convertObjectToArray(tableState.search.predicateObject)[0];
				search = search === undefined ? "" : search;

				if (firstTime) {
					firstTime = false;
					sortBy = 'desc';
					sortField = 'grp_dttm';
				} else if (sortBy == '') {
					sortBy = 'desc';
				}

				var tempField = Util.filterField(sortField);

				Service.listGroup(start, dataPerPage, tempField, sortBy, search, $scope.groupFilter).then(
					function successCallback(response) {
						var resp = response.data;
						$scope.rowCollection = resp.data;
						tableState.pagination.numberOfPages = resp.numberOfPages;
						$scope.numberOfPages = resp.numberOfPages;
						tableState.sort.predicate = sortField;
						$scope.isLoading = false;

						$scope.descriptions = [];
						for (var obj of $scope.rowCollection) {
							obj.created_datetime = new Date(obj.created_datetime + " UTC").getTime();
							$scope.descriptions.push({ no: obj.no, description: obj.description || "Empty" });
						}
					},
					function errorCallback(error) {
						Util.signinPopup();
					}
				).catch(Service.catch);
			};

		}
	}).directive('stGroupFilter', function ($log, $compile) {
		return {
			restrict: 'E',
			require: '^stTable',
			templateUrl: 'resources/template/input.radio.directive.html',
			scope: {
				filter: '=',
			},
			link: function (scope, element, attr, ctrl) {
				scope.customFilter = scope.filter;
				scope.radioFilter = function () {
					scope.filter = scope.customFilter;
					ctrl.pipe();
				};
				scope.reload = function () {
					ctrl.pipe(); 
				};
												
			}
		};
	})
