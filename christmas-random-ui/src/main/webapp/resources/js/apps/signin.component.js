'use strict';

var app = angular.module('ChristmasRandom');

app.component(
	'signin',
	{
		templateUrl : 'pages/signin.html',
		controller : function ($scope, $location, $http, $q, $route, $localStorage, 
			$rootScope, $log, uuid2, growl, Util, Constant) {

			$scope.allowLogin = false;

			$scope.signin = function () {
				var req = {
					method: 'POST',
					url: Constant.endpoint + '/authen',
					headers: {
						'Accept': 'application/json',
						'bizService': 'authen',
						'Content-Type': 'application/json',
						'timestamp': new Date().getTime(),
						'uuid': uuid2.newuuid()
					},
					data: {
						username : $scope.user.username,
						password : $scope.user.password
					}
				};
				
				$http(req).then(
					function(response){
						var result = response.data;
						$localStorage.user = { 
							username: $scope.user.username, 
							token: result.data.token,
							series: result.data.series,
							role: result.data.roleName,
							firstname: result.data.firstname,
							lastname: result.data.lastname
						};
						if ($localStorage.user.role == "USER" || $localStorage.user.role == "ADMIN") {
							
							$rootScope.user = $localStorage.user;
							$location.path("overview");
						} 
					}, function (error) {
						if(req.data.username == null || req.data.password == null){
							// growl.error('Please fill in the blank.',{title: 'Error!'});
							Util.failPopup('Please fill in the blank.');
						}
						else if(error.data.errorDesc == undefined){
							// growl.warning('Email or password is incorrect.',{title: 'Warning!'});
							Util.failPopup('Email or password is incorrect.');
						}
						else{
							// growl.error(error.data.errorDesc,{title: 'Error!'});
							Util.failPopup(error.data.errorDesc);
						}
					});
			}

			$scope.forgotPasswordPopup =  Util.forgotPasswordPopup;
			
			$scope.keyUpCheckEmail = function(){
				var re = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
				$scope.user = $scope.user || { username: "" }
				$scope.user.username = $scope.user.username || "";
				
				if($scope.user.username.length == 5 && $scope.user.username === 'admin'){
					$scope.statusLogin= "";
					$scope.allowLogin= true;
				}
				else if(re.test($scope.user.username)){
					$scope.statusLogin = "";
					$scope.allowLogin = true;
				}
				else if(re.test($scope.user.username)){
					$scope.statusLogin = "Please insert email pattern !";
					$scope.allowLogin = false;
				}
					
			}
			
		}
});
