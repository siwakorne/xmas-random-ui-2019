var app = angular.module('ChristmasRandom');

app.component(
			'keyword',
			{
				templateUrl : 'pages/keyword.html',
				controller : function ($scope,$location,$http,$localStorage,uuid2,$rootScope,$location,growl) {
					var token = $localStorage.currentUser.token;
					var series = $localStorage.currentUser.series;
					var username = $localStorage.currentUser.username;
					
					$scope.base64Encode = function (str) {
						btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
					        return String.fromCharCode('0x' + p1);
					    }));
					}
				
					$scope.base64Decode = function (str) {
						    return decodeURIComponent(Array.prototype.map.call(atob(str), function(c) {
						        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
						    }).join(''));
					}
					
					$scope.fetchKeyword = function () {
						var req = {
							method: 'GET',
							url: 'http://localhost:8089/christmas-random-api/keyword?username='+username,
							headers: {
						        'Accept': 'application/json',
						        'bizService': 'kywd-by-user',
						        'Content-Type': 'application/json',
						        'timestamp': new Date().getTime(),
						        'token': $localStorage.currentUser.token,
						        'seriesId': series,
						        'uuid': uuid2.newuuid()
						    }
						};
						
						$http(req).then(
							function(response){
								$scope.keyword = $scope.base64Decode(response.data.data.keywordDesc);
								$("#receiveKeyword").append($scope.base64Decode(response.data.data.receiveKeywordDesc));
								if($scope.keyword==null){
									$scope.mode='Add';
								}else{
									$scope.mode='Save';
								}
							 }, function (error) {
								 console.log(req);
								 console.log('error: ', error.data);
								 console.log($localStorage.currentUser);
								 if(error.data.errorCode==='B1004'){
									 $scope.mode='Add';
									 $scope.button = false;
								 }
								    
							});
					}
					//add keyword data
					$scope.addKeyword = function () {
						var req = {
								method: 'POST',
								url: 'http://localhost:8089/christmas-random-api/keyword',
								headers: {
							        'Accept': 'application/json',
							        'bizService': "kywd-create",
							        'Content-Type': "application/json",
							        'timestamp': new Date().getTime(),
							        'token': token,
							        'seriesId': series,
							        'uuid': uuid2.newuuid()
							    },
							    data: { 
							    	keywordDesc : $scope.keyword,
							    	username : username
								}
							};
							$http(req).then(
								function(response){
									growl.success('Success, make it a December to Remember.',{title: 'Success!'});
									$scope.fetchKeyword();
									return response;
								}, function (error) {
									if(req.data.keywordDesc.length>256||error.data.errorCode==='T1000'){
										growl.error('Keyword too long',{title: 'Error!'});
									}
									else{
										growl.error(error.data.errorDesc,{title: 'Error!'});
									}
							});
					}
					//edit keyword data
					$scope.editKeyword = function () {
						var req = {
								method: 'PUT',
								url: 'http://localhost:8089/christmas-random-api/keyword',
								headers: {
							        'Accept': 'application/json',
							        'bizService': "kywd-update",
							        'Content-Type': "application/json",
							        'timestamp': new Date().getTime(),
							        'token': token,
							        'seriesId': series,
							        'uuid': uuid2.newuuid()
							    },
							    data: { 
							    	keywordDesc : $scope.keyword,
							    	username : username
								}
							};
							$http(req).then(
								function(response){
									growl.info('Finished, You have changed your keyword.',{title: 'Info!'});
									return response;
								}, function (error) {
									if(req.data.keywordDesc.length>256||error.data.errorCode==='T1000'){
										growl.error('Keyword too long',{title: 'Error!'});
									}
									else{
										growl.error(error.data.errorDesc,{title: 'Error!'});
									}
							});
					}
					//check add or edit keyword
					$scope.addOrEditKeyword = function () {
						$scope.button=true;
						if($scope.mode==='Add'){
							$scope.addKeyword();
						}else{
							$scope.editKeyword();
						}
					}
					
					//clear keyword
					$scope.clear = function() {
					        $scope.keyword = "";
					};
					
					$rootScope.deleteToken = function() {
						var url = 'http://localhost:8089/christmas-random-api/authen';
						var req = {
							method: 'DELETE',
							headers: {
						        'Accept': 'application/json',
						        'bizService': "logout",
						        'Content-Type': "application/json",
						        'timestamp': new Date().getTime(),
						        'token': token,
						        'seriesId': series,
						        'uuid': uuid2.newuuid()
						    },
						    data: { 
						    	seriesId:series,
						    	username:username
						    }
						};

						$http.delete(url,req).then(function (response) {
							$localStorage.$reset();
							$rootScope.authenticated = false;
							$location.path("/signin");
						    console.log('token have deleted', response.data);
						}, function (error) {
						    console.log('an error occurred', error.data);
						});
						
					}
					
					// Initial
					$scope.fetchKeyword();
				}
});