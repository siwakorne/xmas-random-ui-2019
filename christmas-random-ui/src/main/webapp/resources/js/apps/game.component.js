'use strict';

var app = angular.module('ChristmasRandom');

app.component(
    'game', {
        templateUrl: 'pages/game.html',
        controllerAs: 'game',
        controller: function($scope, $rootScope, $q, $log, $http, Constant, Service,
            Util, $localStorage, $route, $location, $window, $compile, $sce) {

            var encodedStr = function(rawStr) {
                rawStr.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
                    return '&#' + i.charCodeAt(0) + ';';
                });
            }

            var ctrl = this; // fixed: problem value of textarea not update.
            ctrl.leaderData = {};

            $scope.isPlayersEmpty = true;
            $scope.isRequestsEmpty = true;
            $scope.isOwner = false;
            $scope.inviteByEmail = "";

            $scope.backToOverview = function() {
                $location.path('overview').search({});
            };

            $scope.params = $location.search();
            $scope.groupName = $location.search().group_name;
            $scope.groupStatus = null;

            $scope.reload = Util.reload;

            $scope.descriptionUpdateFn = function() {
                if (typeof ctrl.description == "string" && ctrl.description.length > 0) {
                    Service.descriptionUpdate($scope.room_name, ctrl.description, Util).then(
                        function(response) {
                            Util.successPopup("Update description success");
                        },
                        function(error) {
                            Util.failPopup(error);
                        }
                    );
                }
            };

            $scope.keywordForm = function() {
                Service.getKeyword($scope.room_name, Util).then(function(response) {
                    var keyword = response.data.keyword;
                    $scope.keywordPopupForm(keyword || '');
                }, function(error) {
                    if (error == 're-signin') {
                        swal.close();
                        Util.signinPopup();
                    } else {
                        Util.failPopup(error);
                    }
                }).catch(function(error) { $log.info(error) });
            };

            $scope.keywordPopupForm = function(keyword) {
                swal({
                    title: 'Type your keyword here',
                    input: 'textarea',
                    inputValue: keyword,
                    inputClass: 'custom-textarea-swal',
                    // html: '<textarea class="swal2-textarea" style="resize: none;"> ' + keyword + '</textarea>',
                    showCancelButton: true,
                    confirmButtonText: 'Save',
                    cancelButtonText: 'Close',
                    allowEnterKey: false,
                    preConfirm: function(keyword) {

                        return Service.updateKeyword(keyword, $scope.groupName, Util).then(
                            function(response) {
                                $route.reload();
                            },
                            function(error) {
                                console.log(error);
                                if (error == 're-signin') {
                                    swal.close();
                                    Util.signinPopup();
                                } else {
                                    // Util.failPopup(error);
                                    throw error;
                                }
                            }
                        );
                    }
                }).then(function(response) {}).catch(swal.noop);
            };

            $scope.getKeyword = function() {
                Service.getKeyword($scope.room_name, Util).then(
                    function(response) {

                        var utilTmp = {
                            htmlentitiesEncode: function(str) {
                                var buf = [];

                                for (var i = str.length - 1; i >= 0; i--) {
                                    buf.unshift(['&#', str[i].charCodeAt(), ';'].join(''));
                                }
                                return buf.join('');
                            },
                            htmlentitiesDecode: function(str) {
                                return str.replace(/&#(\d+);/g, function(match, dec) {
                                    return String.fromCharCode(dec);
                                });
                            }
                        }
                        swal({
                            type: 'info',
                            title: 'Keyword',
                            html: '<fieldset><legend>Receive keyword:</legend> <i style="font-weight: bold;"><div style="display: inline;" id="receive_keyword"></div></i></fieldset>' +
                                '<fieldset><legend>My keyword:</legend> <i style="font-weight: bold;"><div style="display: inline;" id="my_keyword"></div></i></fieldset>'
                                // text: 'My keywork: ' + response.data.keyword + ', Receive keywork:' + response.data.receive_keyword
                        });
                        $('#my_keyword').text(response.data.keyword);
                        $('#receive_keyword').text(response.data.receive_keyword);

                    },
                    function(error) {});
            };

            $scope.randomStart = function() {
                $scope.groupStatus = 'started';
                $scope.afterRandom = true;
                Service.randomStart($scope.room_name, Util).then(Util.success, Util.error);
            };

            $scope.randomFinish = function() {
                    Service.randomFinish($scope.room_name, Util).then(Util.success, Util.error);
                }
                ///^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
            $scope.inviteByEmailFn = function() {
                if ($scope.inviteByEmail != undefined && $scope.inviteByEmail != null && $scope.inviteByEmail != "" && typeof $scope.inviteByEmail != "object") {
                    Service.requestGroup($scope.room_name, $scope.inviteByEmail, Util).then(
                        function(response) {
                            swal({
                                type: 'success',
                                title: 'Invite success'
                            }).then(Util.success, Util.success).catch(swal.noop);
                        }, Util.error);
                }
            };

            Service.accessGroup($scope.groupName, Util).then(function(response) {

                var data = response.data;
                $scope.status = response.status;
                $scope.groupStatus = data.group_status;
                $scope.keyword_status = data.keyword_status;
                $scope.players = data.players;
                $scope.requests = data.requests;
                $scope.created_by = data.created_by;
                $scope.created_at = new Date(data.created_at + " UTC").getTime();
                $scope.room_name = data.room_name;
                $scope.isPlayersEmpty = $scope.players.length <= 0;
                $scope.isRequestsEmpty = $scope.requests.length <= 0;
                ctrl.description = data.description || "";
                $scope.isOwner = ($rootScope.user.firstname + ' ' + $rootScope.user.lastname) == $scope.created_by;

                var readyTemp = {};
                readyTemp.total = 0;
                angular.forEach($scope.players, function(value, key) {
                    if (value.player_stts == 'yes')
                        this.total++;
                    if (value.player == $scope.created_by)
                        ctrl.leaderData.player_stts = value.player_stts;
                }, readyTemp);
                $scope.ready = readyTemp;
                if (ctrl.leaderData.player_stts != 'yes') {
                    // $scope.ready.total = 0;
                }

            }, function(error) {
                if (error == 're-signin') {
                    Util.signinPopup();
                } else {
                    swal({
                        type: 'error',
                        title: 'Failure',
                        html: error
                    }).then(function() {
                        $window.location.href = "#!/overview";
                    }, function() {
                        $window.location.href = "#!/overview";
                    }).catch(swal.noop);
                }
            });

            $scope.acceptAll = function(groupName) {
                Service.acceptAll(groupName, Util).then(function(response) {
                    if (response.status == 0) {
                        Util.successPopup('Accepted all player requests in ' + groupName + ' game.');
                    }
                }, function(error) {
                    if (error == 're-signin') {
                        swal.close();
                        Util.signinPopup();
                    } else {
                        Util.failPopup('Failed to accept all player requests in ' + groupName + ' game.');
                    }
                });
            };

            $scope.rejectAll = function(groupName) {
                Service.rejectAll(groupName, Util).then(function(response) {
                    if (response.status == 0) {
                        Util.successPopup('Rejected all player requests in ' + groupName + ' game.');
                    }
                }, function(error) {
                    if (error == 're-signin') {
                        swal.close();
                        Util.signinPopup();
                    } else {
                        Util.failPopup('Failed to reject all player requests in ' + groupName + ' game.');
                    }
                });
            };

            $scope.accept = function(groupName, playerId, playerName) {
                Service.accept(groupName, playerId, Util).then(function(response) {
                    if (response.status == 0) {
                        Util.successPopup(playerName + ' has been accepted in ' + groupName + ' game.');
                    }
                }, function(error) {
                    if (error == 're-signin') {
                        swal.close();
                        Util.signinPopup();
                    } else {
                        Util.failPopup('Failed to accept ' + playerName + ' in ' + groupName + ' game.');
                    }
                });
            };

            $scope.reject = function(groupName, playerId, playerName) {
                Service.reject(groupName, playerId, Util).then(function(response) {
                    if (response.status == 0) {
                        Util.successPopup(playerName + ' has been rejected in ' + groupName + ' game.');
                    }
                }, function(error) {
                    if (error == 're-signin') {
                        swal.close();
                        Util.signinPopup();
                    } else {
                        Util.failPopup('Failed to reject ' + playerName + ' in ' + groupName + ' game.');
                    }
                });
            };
        }
    }
);