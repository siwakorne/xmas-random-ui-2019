var app = angular.module('ChristmasRandom');

app.component(
    'signup', {
        templateUrl: 'pages/signup.html',
        controllerAs: 'ctrl',
        controller: function($scope, $rootScope, $location, $http, uuid2,
            $localStorage, growl, Constant, $log, $route, vcRecaptchaService, $timeout, Util) {
            $scope.siteKey = Constant.reCaptchaV2.siteKey;
            $scope.widgetId = null;
            $scope.response = null;
            $scope.allowSignup = false;

            $scope.setWidgetId = function(widgetId) {
                $scope.widgetId = widgetId;
            };

            $scope.setResponse = function(response) {
                console.log(response);
                // if (vcRecaptchaService.getResponse() === '') {
                //     alert('Null');
                // } else {
                //     alert(vcRecaptchaService.getResponse());
                // }
                $scope.response = response;
            };

            $scope.cbExpiration = function() {
                vcRecaptchaService.reload($scope.widgetId);
                $scope.response = null;
            };

            $scope.signup = function() {

                var req = {
                    method: 'POST',
                    url: Constant.endpoint + '/user',
                    headers: {
                        'Accept': 'application/json',
                        'bizService': "create-user",
                        'Content-Type': "application/json",
                        'timestamp': new Date().getTime(),
                        'uuid': uuid2.newuuid()
                    },
                    data: {
                        username: $scope.user.email,
                        password: $scope.user.password,
                        firstName: $scope.user.name,
                        lastName: $scope.user.lastName
                    }
                };

                Util.verifyCaptchaPopup($scope.user, $scope.response, Util, Constant);
            }

            // not using in ver-1.2
            $scope.autoLogin = function() {
                var req = {
                    method: 'POST',
                    url: Constant.endpoint + '/authen',
                    headers: {
                        'Accept': 'application/json',
                        'bizService': 'authen',
                        'Content-Type': 'application/json',
                        'timestamp': new Date().getTime(),
                        'uuid': uuid2.newuuid()
                    },
                    data: {
                        username: $scope.user.email,
                        password: $scope.user.password,
                    }
                };
                $http(req).then(
                    function(response) {
                        var result = response.data;
                        $localStorage.user = {
                            username: $scope.user.email,
                            token: result.data.token,
                            series: result.data.series,
                            role: result.data.roleName,
                            firstname: $scope.user.name,
                            lastname: $scope.user.lastName
                        };

                        if ($localStorage.user.role == "USER" || $localStorage.user.role == "ADMIN") {
                            $rootScope.role = $localStorage.user.role;
                            $rootScope.user = $localStorage.user;
                            $location.path("overview");
                        }

                    },
                    function(error) {
                        if (req.data.username == null || req.data.password == null) {
                            growl.error('Please fill in the blank.', { title: 'Error!' });
                        } else if (error.data.errorDesc == undefined) {
                            growl.warning('Email or password is incorrect.', { title: 'Warning!' });
                        } else {
                            growl.error(error.data.errorDesc, { title: 'Error!' });
                        }
                        $location.path('signin');
                    }).catch(function(error) {
                    $log.error(error);
                });
            }

            var timeoutObj = null;

            $scope.keyUpCheckEmail = function() {
                var re = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
                $scope.emailValid = "";
                $timeout.cancel(timeoutObj);
                // console.log($scope.signupForm.password.$error.required);
                timeoutObj = $timeout(function() {
                    if (re.test($scope.user.email)) {

                        $scope.emailValid = "";
                        $scope.allowSignup = true;
                    } else {
                        $scope.emailValid = typeof $scope.user.email == "string" && $scope.user.email.trim() != "" ? "Please insert email pattern !" : "";
                        $scope.allowSignup = false;
                    }
                }, 1500);

            }
        }

    });



app.directive("matchPassword", function() {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=matchPassword"
        },
        link: function(scope, element, attributes, ngModel) {

            ngModel.$validators.matchPassword = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
});