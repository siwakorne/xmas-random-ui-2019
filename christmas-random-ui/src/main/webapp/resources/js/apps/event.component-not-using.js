/* not using */
"use strict";

var app = angular.module("ChristmasRandom");

app.component(
	"event",
	{
		templateUrl: "pages/event.html",
		controller: function ($scope, $rootScope, $log, $timeout, Constant, Service, Util) {

			var firstTime = true;
			
			$rootScope.$broadcast ("routePermission", { route: 'event' })

			$scope.numberOfPages = Constant.default.numberOfPages; // default
			$scope.currentTab = "my-group"; // value: my-group | my-event | request-pending

			$scope.tabChange = function (tab) {
				$scope.currentTab = tab;
			};

			$scope.callService = function (tableState) {
				$scope.isLoading = true;
				var start = tableState.pagination.start;
				var dataPerPage = tableState.pagination.number;
				var sortBy = Util.isPropExist(tableState.sort, 'predicate') < 0 ? '' : Util.convertSortName(tableState.sort.reverse);
				var sortField = Util.isPropExist(tableState.sort, 'reverse') < 0 ? '' : tableState.sort.predicate;
				var search = Util.convertObjectToArray(tableState.search.predicateObject)[0];
				
				if(firstTime) {
					firstTime = false;
					sortBy = 'desc';
					sortField = 'grp_dttm';
				}
				
				search = search === undefined ? "" : search;

				var tempField = Util.filterField(sortField);
				
				Service.listGroup(start, dataPerPage, tempField, sortBy, search).then(
					function successCallback(response) {
						var resp = response.data;
						$scope.rowCollection = resp.data;
						tableState.pagination.numberOfPages = resp.numberOfPages;
						$scope.numberOfPages = resp.numberOfPages;
						tableState.sort.predicate = sortField;
						$scope.isLoading = false;
					},
					function errorCallback(error) {
						Util.signinPopup();
					}	
				).catch(Service.catch);
				
			};

		}
	}
);