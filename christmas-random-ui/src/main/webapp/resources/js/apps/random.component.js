var app = angular.module('ChristmasRandom');

app.component(
			'random',
			{
				templateUrl : 'pages/random.html',
				controller : function ($scope, $http,$localStorage,uuid2,$rootScope,$location,$route,growl) {
					var token = $localStorage.currentUser.token;
					var series = $localStorage.currentUser.series;
					var username = $localStorage.currentUser.username;
					$scope.fetch = function () {
						var req = {
							method: 'GET',
							url: 'http://localhost:8089/christmas-random-api/user',
							headers: {
						        'Accept': 'application/json',
						        'bizService': 'all-user',
						        'Content-Type': 'application/json',
						        'uuid': uuid2.newuuid(),
						        'timestamp': new Date().getTime(),
						        'token': token,
						        'seriesId': series,
						        'uuid': uuid2.newuuid()
						    }
						};
						
						var reqKeys = {
								method: 'GET',
								url: 'http://localhost:8089/christmas-random-api/user',
								headers: {
							        'Accept': 'application/json',
							        'bizService': 'all-key',
							        'Content-Type': 'application/json',
							        'uuid': uuid2.newuuid(),
							        'timestamp': new Date().getTime(),
							        'token': token,
							        'seriesId': series,
							        'uuid': uuid2.newuuid()
							    }
							};
	
						$http(req).then(
							function(response){
								$scope.data = response.data.data;
								
								angular.forEach($scope.data, function(value, key){
							         if(value.keywordFlag == 'N')
							           $scope.keyNo=true;
							    });
								
								
								
							 }, function (error) {
								 	growl.error(error.data,{title: 'Error!'});
							});
						
						$http(reqKeys).then(
								function(response){
									if(response.data.data.randState == 1) {
										$scope.keyNo=true;
										$("#started").css("display", "block");
										$("#finish").css("display", "block");
									}
										
								 }, function (error) {
									 	growl.error(error.data,{title: 'Error!'});
								});
					}
					
					$scope.delete = function (id) {
						var url = 'http://localhost:8089/christmas-random-api/user?userId='+ id;
						var req = {
							method: 'DELETE',
							headers: {
						        'Accept': 'application/json',
						        'bizService': "delete-user",
						        'Content-Type': "application/json",
						        'timestamp': new Date().getTime(),
						        'token': token,
						        'seriesId': series,
						        'uuid': uuid2.newuuid()
						    },
						    data: { 
						    	userId:id
						    }
						};
	
						$http.delete(url,req).then(function (response) {
							$route.reload();
						}, function (error) {
							growl.error(error.data,{title: 'Error!'});
						});
					}
					
					$scope.random = function () {
						var req = {
							method: 'GET',
							url: 'http://localhost:8089/christmas-random-api/random',
							headers: {
						        'Accept': 'application/json',
						        'bizService': 'random',
						        'Content-Type': 'application/json',
						        'timestamp': new Date().getTime(),
						        'token': token,
						        'seriesId': series,
						        'uuid': uuid2.newuuid()
						    }
						};
						
	
						$http(req).then(
							function(response){
								if(response.data.data.randState == 1) $scope.keyNo = true;
								location.reload();
							 }, function (error) {
								    console.log('an error occurred', error.data);
							});
						
						
					}
					
					$scope.finish = function() {
						var req = {
								method: 'GET',
								url: 'http://localhost:8089/christmas-random-api/random',
								headers: {
							        'Accept': 'application/json',
							        'bizService': 'finish',
							        'Content-Type': 'application/json',
							        'timestamp': new Date().getTime(),
							        'token': token,
							        'seriesId': series,
							        'uuid': uuid2.newuuid()
							    }
							};
						
						$http(req).then(
								function(response){
									location.reload();
								 }, function (error) {
									    console.log('an error occurred', error.data);
								});
					}
					
					$rootScope.deleteToken = function() {
						var url = 'http://localhost:8089/christmas-random-api/authen';
						var req = {
							method: 'DELETE',
							headers: {
						        'Accept': 'application/json',
						        'bizService': "logout",
						        'Content-Type': "application/json",
						        'timestamp': new Date().getTime(),
						        'token': token,
						        'seriesId': series,
						        'uuid': uuid2.newuuid()
						    },
						    data: { 
						    	seriesId:series,
						    	username:username
						    }
						};

						$http.delete(url,req).then(function (response) {
							$localStorage.$reset();
							$rootScope.authenticated = false;
							$location.path("/signin");
						    console.log('token have deleted', response.data);
						}, function (error) {
						    growl.error(error.data,{title: 'Error!'});
						});
						
					}
					// Initial
					$scope.fetch();
				}
});