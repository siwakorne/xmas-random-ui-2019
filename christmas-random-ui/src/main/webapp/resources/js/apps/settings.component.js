'use strict';

var app = angular.module('ChristmasRandom');

app.component(
	'settings',
	{
		templateUrl: 'pages/settings.html',
		controller: function ($scope, $log, $location, $localStorage, $rootScope, $route, Service, Util) {

			$scope.signupSubmitted = false;

			$scope.firstname = $rootScope.user.firstname;
			$scope.lastname = $rootScope.user.lastname;

			$scope.changePwd = function (new_password, old_password, firstname, lastname) {

				$scope.signupSubmitted = new_password != undefined && old_password != undefined;

				Service.changePassword(new_password, old_password, firstname, lastname, Util).then(function (response) {
					var isUpdateProfile = (typeof firstname == "string" && firstname.length > 0) && (typeof lastname == "string" && lastname.length > 0);
					if (isUpdateProfile) {
						$localStorage.user.firstname = firstname;
						$localStorage.user.lastname = lastname;
					}
					swal({
						title: 'Success',
						type: 'success',
						html: 'Your password has been changed successfully',
						showConfirmButton: false,
						showCancelButton: true,
						cancelButtonText: 'Close',
					}).then(function (response) {
						
					 }, function (dismiss) {
						$route.reload();
					 }).catch(swal.noop);

				}, function (error) {
					if (error=='re-signin') {
						Util.signinPopup();
					} else {
						swal({
							title: 'Failed',
							type: 'error',
							html: error,
							showConfirmButton: false,
							timer: 5000,
							onOpen: function () { $route.reload(); }
						}).catch(swal.noop);
					}
				}).catch(function(error){
					$log.error(error);
				});
			};
		}
	}
);
