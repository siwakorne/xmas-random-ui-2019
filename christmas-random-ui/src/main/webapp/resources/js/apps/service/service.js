'use strict';

var app = angular.module('ChristmasRandom');

app.factory('Service', ['$http', '$q', '$log', 'uuid2', 'Constant', '$localStorage', '$window',
    function($http, $q, $log, uuid2, Constant, $localStorage, $window) {

        var obj = {
            signin: function(username, password) {
                var req = {
                    method: 'POST',
                    url: Constant.endpoint + '/authen',
                    data: {
                        username: username,
                        password: password
                    }
                };
                req.headers = obj.defaultHeader();
                req.headers.bizService = 'authen';
                var deferred = $q.defer();

                if (typeof password == "string" && password.length > 0)
                    $http(req).then(function(response) {
                        deferred.resolve(response.data);
                    }, function(error) {
                        deferred.reject("Password is incorrect");
                    }).catch(obj.catch);
                else
                    deferred.reject("Enter your password please.");

                return deferred.promise;
            },
            forgotPassword: function(email, Util) {
                var req = {
                    method: 'GET',
                    url: Constant.endpoint + '/user',
                    params: {
                        username: email
                    }
                };
                req.headers = obj.defaultHeader();
                req.headers.bizService = 'forget-password';
                var deferred = $q.defer();

                $http(req).then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    var status = error.status;
                    if (Constant.errorCodes.indexOf(error.data.errorCode) > -1) {
                        deferred.reject(error.data.errorDesc);
                    } else {
                        swal.close();
                        Util.unexpectErrorPopup();
                    }
                }).catch(obj.catch);

                return deferred.promise;
            },
            reCaptcha: function(user, captchaResource, Util) { // reCaptcha v.2
                var req = {
                    method: 'POST',
                    url: Constant.endpoint + '/user',
                    headers: {
                        'Accept': 'application/json',
                        'bizService': "create-user",
                        'Content-Type': "application/json",
                        'timestamp': new Date().getTime(),
                        'uuid': uuid2.newuuid()
                    },
                    data: {
                        username: user.email,
                        password: user.password,
                        firstName: user.name,
                        lastName: user.lastName
                    }
                };
                if (captchaResource === "") {
                    swal("Please resolve the captcha and signup!").catch(swal.noop);
                    return;
                } else {
                    req.data['g-recaptcha-response'] = captchaResource;

                    var deferred = $q.defer();
                    $http(req).then(
                        function(response) {
                            deferred.resolve("captcha success");
                        },
                        function(error) {
                            var errorCode = error.data.errorCode;
                            var errorDesc = error.data.errorDesc;
                            if (Constant.errorCodes.indexOf(errorCode) > -1) {
                                deferred.reject(error.data);
                            } else {
                                swal.close();
                                Util.unexpectErrorPopup();
                            }
                        }).catch(function(error) {
                        $log.error(error);
                    });
                    return deferred.promise;
                }
            },
            listGroup: function(start, number, sortField, sortBy, search, groupFilter) {
                var req = {
                    method: 'GET',
                    url: Constant.endpoint + '/groups',
                    params: {
                        start: start,
                        data_per_page: number,
                        sort_field: sortField,
                        sort_by: sortBy,
                        search: search
                    }
                };
                req.headers = obj.defaultHeader();
                req.headers.bizService = groupFilter;
                return $http(req);
            },
            createGroup: function(groupName, Util) {
                var req = {
                    url: Constant.endpoint + '/groups',
                    data: { group_name: groupName },
                    method: 'POST',
                };
                req.headers = obj.defaultHeader();
                req.headers.bizService = 'create-group';
                var deferred = $q.defer();

                if (typeof groupName == "string" && groupName.length > 0) {
                    $http(req).then(function(response) {
                        deferred.resolve(response.data);
                    }, function(error) {
                        var status = error.status;
                        error.data.errorCode = error.data.errorCode || null;
                        if (status == 401 || status == 403) {
                            deferred.reject("re-signin");
                        } else if (Constant.errorCodes.indexOf(error.data.errorCode) > -1) {
                            deferred.reject(error.data.errorDesc + ": \"" + groupName + "\"");
                        } else {
                            swal.close();
                            Util.unexpectErrorPopup();
                        }
                    }).catch(obj.catch);
                } else {
                    deferred.reject("Enter your group name please.");
                }

                return deferred.promise;
            },
            requestGroup: function(groupName, inviteByEmail, Util) {
                var req = {
                    url: Constant.endpoint + '/groups',
                    method: 'GET',
                    params: { group_name: groupName, invite_by_email: inviteByEmail }
                };
                req.headers = obj.defaultHeader();
                req.headers.bizService = 'request-group';
                var deferred = $q.defer();

                $http(req).then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    var status = error.status;
                    error.data.errorCode = error.data.errorCode || null;
                    if (status == 401 || status == 403) {
                        deferred.reject("re-signin");
                    } else if (Constant.errorCodes.indexOf(error.data.errorCode) > -1) {
                        deferred.reject(error.data.errorDesc);
                    } else {
                        swal.close();
                        Util.unexpectErrorPopup();
                    }
                }).catch(obj.catch);
                return deferred.promise;
            },
            accessGroup: function(groupName, Util) {
                var req = {
                    url: Constant.endpoint + '/event',
                    method: 'GET',
                    params: { group_name: groupName }
                };
                req.headers = obj.defaultHeader();
                req.headers.bizService = 'event-players-list';
                var deferred = $q.defer();

                $http(req).then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    var status = error.status;
                    $log.error(error);
                    error.data = error.data || {};
                    var errorCode = error.data.errorCode || null;
                    if (status == 401 || status == 403) {
                        deferred.reject("re-signin");
                    } else if (Constant.errorCodes.indexOf(errorCode) > -1) {
                        deferred.reject(error.data.errorDesc + ": " + groupName);
                    } else {
                        Util.unexpectErrorPopup();
                    }
                }).catch(obj.catch);
                return deferred.promise;
            },
            accept: function(groupName, user_id, Util) {
                var req = {
                    method: 'GET',
                    url: Constant.endpoint + '/groups',
                    params: {
                        group_name: groupName,
                        user_id: user_id,
                        accept: 'accept'
                    }
                };
                req.headers = obj.defaultHeader();
                req.headers.bizService = 'accept-request-group';
                var deferred = $q.defer();

                $http(req).then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    var status = error.status;
                    error.data.errorCode = error.data.errorCode || null;
                    if (status == 401 || status == 403) {
                        deferred.reject("re-signin");
                    } else if (Constant.errorCodes.indexOf(error.data.errorCode) > -1) {
                        deferred.reject(error.data.errorDesc + ": \"" + groupName + "\"");
                    } else {
                        swal.close();
                        Util.unexpectErrorPopup();
                    }
                }).catch(obj.catch);
                return deferred.promise;
            },
            reject: function(groupName, user_id, Util) {
                var req = {
                    method: 'GET',
                    url: Constant.endpoint + '/groups',
                    params: {
                        group_name: groupName,
                        user_id: user_id,
                        accept: 'delete'
                    }
                };
                req.headers = obj.defaultHeader();
                req.headers.bizService = 'accept-request-group';
                var deferred = $q.defer();

                $http(req).then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    var status = error.status;
                    error.data.errorCode = error.data.errorCode || null;
                    if (status == 401 || status == 403) {
                        deferred.reject("re-signin");
                    } else if (Constant.errorCodes.indexOf(error.data.errorCode) > -1) {
                        deferred.reject(error.data.errorDesc + ": \"" + groupName + "\"");
                    } else {
                        swal.close();
                        Util.unexpectErrorPopup();
                    }
                }).catch(obj.catch);;
                return deferred.promise;
            },
            acceptAll: function(groupName, Util) {
                var req = {
                    method: 'GET',
                    url: Constant.endpoint + '/groups',
                    params: {
                        group_name: groupName,
                        accept: 'accept-all'
                    }
                };
                req.headers = obj.defaultHeader();
                req.headers.bizService = 'accept-request-group';
                var deferred = $q.defer();

                $http(req).then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    var status = error.status;
                    error.data.errorCode = error.data.errorCode || null;
                    if (status == 401 || status == 403) {
                        deferred.reject("re-signin");
                    } else if (Constant.errorCodes.indexOf(error.data.errorCode) > -1) {
                        deferred.reject(error.data.errorDesc + ": \"" + groupName + "\"");
                    } else {
                        swal.close();
                        Util.unexpectErrorPopup();
                    }
                }).catch(obj.catch);;
                return deferred.promise;
            },
            rejectAll: function(groupName, Util) {
                var req = {
                    method: 'GET',
                    url: Constant.endpoint + '/groups',
                    params: {
                        group_name: groupName,
                        accept: 'delete-all'
                    }
                };
                req.headers = obj.defaultHeader();
                req.headers.bizService = 'accept-request-group';
                var deferred = $q.defer();

                $http(req).then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    var status = error.status;
                    error.data.errorCode = error.data.errorCode || null;
                    if (status == 401 || status == 403) {
                        deferred.reject("re-signin");
                    } else if (Constant.errorCodes.indexOf(error.data.errorCode) > -1) {
                        deferred.reject(error.data.errorDesc + ": \"" + groupName + "\"");
                    } else {
                        swal.close();
                        Util.unexpectErrorPopup();
                    }
                }).catch(obj.catch);
                return deferred.promise;
            },
            updateKeyword: function(keyword, groupName, Util) {
                var req = {
                    method: 'GET',
                    url: Constant.endpoint + '/event',
                    params: {
                        keyword: keyword,
                        group_name: groupName,
                    }
                };
                req.headers = obj.defaultHeader();
                req.headers.bizService = 'event-edit-kywd';
                var deferred = $q.defer();

                $http(req).then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    var status = error.status;
                    error.data.errorCode = error.data.errorCode || null;
                    if (status == 401 || status == 403) {
                        deferred.reject("re-signin");
                    } else if (Constant.errorCodes.indexOf(error.data.errorCode) > -1) {
                        deferred.reject(error.data.errorDesc);
                    } else {
                        swal.close();
                        Util.unexpectErrorPopup();
                    }
                }).catch(obj.catch);
                return deferred.promise;
            },
            getKeyword: function(groupName, Util) {
                var req = {
                    method: 'GET',
                    url: Constant.endpoint + '/event',
                    params: {
                        group_name: groupName,
                    }
                };
                req.headers = obj.defaultHeader();
                req.headers.bizService = 'event-get-keyword';
                var deferred = $q.defer();

                $http(req).then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    var status = error.status;
                    error.data.errorCode = error.data.errorCode || null;
                    if (status == 401 || status == 403) {
                        deferred.reject("re-signin");
                    } else if (Constant.errorCodes.indexOf(error.data.errorCode) > -1) {
                        deferred.reject(error.data.errorDesc);
                    } else {
                        swal.close();
                        Util.unexpectErrorPopup();
                    }
                }).catch(obj.catch);
                return deferred.promise;
            },
            changePassword: function(new_password, old_password, firstname, lastname, Util) {
                var req = {
                    method: 'PUT',
                    url: Constant.endpoint + '/user',
                    data: {
                        old_password: old_password,
                        new_password: new_password,
                        first_name: firstname,
                        last_name: lastname
                    }
                };
                req.headers = obj.defaultHeader();
                var deferred = $q.defer();
                $http(req).then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    var status = error.status;
                    error.data = error.data || {};
                    error.data.errorCode = error.data.errorCode || null;
                    if (status == 401 || status == 403) {
                        deferred.reject("re-signin");
                    } else if (Constant.errorCodes.indexOf(error.data.errorCode) > -1) {
                        deferred.reject(error.data.errorDesc);
                    } else {
                        swal.close();
                        Util.unexpectErrorPopup();
                    }
                }).catch(obj.catch);
                return deferred.promise;
            },
            getKeyword: function(groupName, Util) {
                var req = {
                    method: 'GET',
                    url: Constant.endpoint + '/event',
                    params: { group_name: groupName }
                };
                req.headers = obj.defaultHeader();
                req.headers.bizService = 'event-get-keyword';
                var deferred = $q.defer();

                $http(req).then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    var status = error.status;
                    error.data = error.data || {};
                    error.data.errorCode = error.data.errorCode || null;
                    if (status == 401 || status == 403) {
                        deferred.reject("re-signin");
                    } else if (Constant.errorCodes.indexOf(error.data.errorCode) > -1) {
                        deferred.reject(error.data.errorDesc);
                    } else {
                        swal.close();
                        Util.unexpectErrorPopup();
                    }
                }).catch(obj.catch);
                return deferred.promise;
            },
            randomStart: function(game, Util) {
                var req = {
                    url: Constant.endpoint + '/random',
                    method: 'GET',
                    params: { group_name: game }
                };
                req.headers = obj.defaultHeader();
                req.headers.bizService = 'event-start';
                var deferred = $q.defer();

                $http(req).then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    var status = error.status;
                    $log.error(error);
                    error.data = error.data || {};
                    var errorCode = error.data.errorCode || null;
                    if (status == 401 || status == 403) {
                        deferred.reject("re-signin");
                    } else if (Constant.errorCodes.indexOf(errorCode)) {
                        deferred.reject(error.data.errorDesc);
                    } else {
                        Util.unexpectErrorPopup();
                    }
                }).catch(obj.catch);
                return deferred.promise;
            },
            randomFinish: function(game, Util) {
                var req = {
                    url: Constant.endpoint + '/random',
                    method: 'GET',
                    params: { group_name: game }
                };
                req.headers = obj.defaultHeader();
                req.headers.bizService = 'event-finish';
                var deferred = $q.defer();

                $http(req).then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    var status = error.status;
                    $log.error(error);
                    error.data = error.data || {};
                    var errorCode = error.data.errorCode || null;
                    if (status == 401 || status == 403) {
                        deferred.reject("re-signin");
                    } else if (Constant.errorCodes.indexOf(errorCode) > -1) {
                        deferred.reject(error.data.errorDesc);
                    } else {
                        Util.unexpectErrorPopup();
                    }
                }).catch(obj.catch);
                return deferred.promise;
            },
            defaultHeader: function() {
                $localStorage.user = $localStorage.user || {};
                $localStorage.user.token = $localStorage.user.token || "";
                $localStorage.user.series = $localStorage.user.series || "";
                return {
                    'Accept': 'application/json',
                    'Content-Type': "application/json",
                    'timestamp': new Date().getTime(),
                    'token': $localStorage.user.token,
                    'seriesId': $localStorage.user.series,
                    'uuid': uuid2.newuuid()
                }
            },
            descriptionUpdate: function(game, description, Util) {
                var req = {
                    url: Constant.endpoint + '/groups',
                    method: 'PUT',
                    data: {
                        group_name: game,
                        description: description
                    }
                };
                req.headers = obj.defaultHeader();
                req.headers.bizService = 'group-note';
                var deferred = $q.defer();

                $http(req).then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    var status = error.status;
                    error.data = error.data || {};
                    var errorCode = error.data.errorCode || null;
                    if (status == 401 || status == 403) {
                        deferred.reject("re-signin");
                    } else if (Constant.errorCodes.indexOf(errorCode) > -1) {
                        deferred.reject(error.data.errorDesc);
                    } else {
                        Util.unexpectErrorPopup();
                    }
                }).catch(obj.catch);

                return deferred.promise;
            },
            catch: function(error) {
                $log.error(error);
            }
        };
        return obj;
    }
]);