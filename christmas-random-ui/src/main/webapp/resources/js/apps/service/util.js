'use strict';

var app = angular.module('ChristmasRandom');

app.factory('Util', ['$q', '$log', '$localStorage', '$route', 'Service', '$window', '$rootScope', '$timeout',
    function($q, $log, $localStorage, $route, Service, $window, $scope, $rootScope, $timeout) {

        var obj = {
            htmlentitiesEncode: function(str) {
                var buf = [];

                for (var i = str.length - 1; i >= 0; i--) {
                    buf.unshift(['&#', str[i].charCodeAt(), ';'].join(''));
                }
                return buf.join('');
            },
            htmlentitiesDecode: function(str) {
                return str.replace(/&#(\d+);/g, function(match, dec) {
                    return String.fromCharCode(dec);
                });
            },
            forgotPasswordPopup: function() {
                swal({
                    input: 'email',
                    title: 'Reset your password',
                    inputPlaceholder: 'Enter your email address',
                    showLoaderOnConfirm: true,
                    preConfirm: function(email) {
                        return Service.forgotPassword(email, obj).then(
                            function(reponse) {
                                return "success";
                            },
                            function(error) {
                                throw error;
                            }
                        );
                    }
                }).then(function(result) {
                        if (result == 'success') {
                            swal({
                                type: 'success',
                                html: 'Check your email inbox for an email with <br/>the subject <label><i>"[Xmas] Reset your password"</i></label>'
                            });
                        }
                    },
                    function(dismiss) {}).catch(function(error) {
                    // console.log(error);
                });
            },
            createGroupPopup: function() {
                swal({
                    title: 'Create Christmasrandom game',
                    input: 'text',
                    inputPlaceholder: 'Enter your game name',
                    showCancelButton: true,
                    preConfirm: function(value) {
                        return Service.createGroup(value, obj).then(
                            function(data) {},
                            function(error) {
                                if (error == "re-signin") {
                                    swal.close();
                                    obj.signinPopup();
                                    throw "session timeout";
                                } else
                                    throw error;
                            });
                    }
                }).then(function(name) {
                    $route.reload();
                    swal({
                        type: 'success',
                        title: 'Created, ' + name + ' game.'
                    })
                }, function(dismiss) {}).catch(swal.noop);
            },
            signinPopup: function() {
                var usernameTmp = $localStorage.user.username;
                // if (!$localStorage.user.username) {  
                //     $window.location.href = '#!/signin';
                //     return;
                // }
                obj.clearUser();
                swal({
                    title: 'Session timeout, <br/><small>Signin your account again please.</small>',
                    input: 'password',
                    inputPlaceholder: 'Enter your password',
                    showCancelButton: true,
                    onOpen: function() {},
                    preConfirm: function(data) {

                        return Service.signin(usernameTmp, data).then(
                            function(response) {
                                var result = response.data;
                                $localStorage.user = {
                                    username: usernameTmp,
                                    firstname: result.firstname,
                                    lastname: result.lastname,
                                    token: result.token,
                                    series: result.series,
                                    role: result.roleName
                                };
                                $route.reload();
                                return response;
                            },
                            function(error) {
                                throw error;
                            }
                        );
                    }
                }).then(function(data) {
                    var fullname = data.data.firstname + " " + data.data.lastname;
                    swal({
                        type: 'success',
                        title: 'Logged, <br/><small>Hi ' + fullname + '</small>'
                    })
                }, function(dismiss) {
                    $window.location.href = '#!/signin?r=signout';
                }).catch(swal.noop);
            },
            verifyCaptchaPopup: function(user, response, Util, Constant) {
                swal({
                    title: 'Verifying...',
                    showLoaderOnConfirm: true,
                    onOpen: function() {
                        swal.clickConfirm();
                    },
                    preConfirm: function() {
                        return Service.reCaptcha(user, response, Util).then(
                            function(response) {
                                return response;
                            },
                            function(error) {
                                return error;
                            }
                        );
                    }
                }).then(function(result) {
                    var type = null;
                    var errorCode = result.errorCode;
                    var errorDesc = result.errorDesc;
                    var results = ["captcha success", "Captcha have problem"];
                    if (result == "captcha success") {
                        type = "success";
                        result = "Signup success, check your email please.";
                        results[0] = result;
                    } else if (result == "Captcha have problem") {
                        type = "error";
                    }
                    if (type == "success") {
                        swal({
                            type: type,
                            title: "Signup succes, check your email and click link confirm please.",
                            showConfirmButton: false,
                            showCancelButton: true,
                            cancelButtonText: "Close"
                        }).then(function(response) {}, function(dismiss) {
                            $window.location.href = '#!/signin';
                        }).catch(swal.noop);
                    } else if (errorCode == 'B1011') {
                        // $('input[type=password]').val('');
                        swal({
                            type: type,
                            title: errorDesc,
                            showConfirmButton: false,
                            showCancelButton: true,
                            cancelButtonText: "Close"
                        }).then(function(response) {}, function(dismiss) {
                            // $location.path("signin").search({});
                        }).catch(swal.noop);
                    } else if (Constant.errorCodes.indexOf(errorCode) > -1) {
                        swal({
                            type: type,
                            title: result,
                            showConfirmButton: false,
                            showCancelButton: true,
                            cancelButtonText: "Close"
                        }).then(function(response) {}, function(dismiss) {
                            $location.path("signin").search({});
                        }).catch(swal.noop);
                    } else {
                        obj.unexpectErrorPopup();
                    }
                });
            },
            accessGroup: function($scope) {
                // Service.accessGroup($scope.groupName).then(function (response) {

                //     var data = response.data;
                //     $scope.status = response.status;
                //     $scope.players = data.players;
                //     $scope.requests = data.requests;
                //     $scope.created_by = data.created_by;
                //     $scope.created_at = data.created_at;
                //     $scope.room_name = data.room_name;
                //     $scope.isPlayersEmpty = $scope.players.length <= 0;
                //     $scope.isRequestsEmpty = $scope.requests.length <= 0;

                //     var readyTemp = {};
                //     readyTemp.total = 0;
                //     angular.forEach($scope.players, function (value, key) {
                //         if(value.player_stts=='yes')
                //             this.total++; 
                //     }, readyTemp);
                //     $scope.ready = readyTemp;

                // }, function (error) {
                //     if (error == 're-signin') {
                //         obj.signinPopup();
                //         throw "session timeout";
                //     } else {
                //         $log.error(error);
                //     }
                // });
            },
            accessGroupPopup: function(groupName, groupData) {
                var roomHtml = obj.getInnerHTMLById("room-element");
                $scope.isLoadingPopup = true;
                Service.accessGroup(groupName, obj).then(
                    function(response) {
                        $scope.isLoadingPopup = false;
                        var data = response.data;
                        $rootScope.rowPlayersCollection = data.players;
                        groupData = data;
                        swal({
                            html: roomHtml,
                            target: 'div#overview-popup',
                            width: '60%',
                            showCloseButton: true,
                            showCancelButton: true,
                            focusConfirm: false,
                            focusCancel: false,
                            confirmButtonColor: '#C2121F',
                            confirmButtonText: 'Play',
                            cancelButtonText: 'Close',
                            onBeforeOpen: function() {

                            },
                            onOpen: function() {
                                // $rootScope.$broadcast('roomName', data.room_name);
                                var minDate = new Date();
                                minDate.setDate(minDate.getDate() + 1);
                                $('.roomName').html(data.room_name);
                                $('.finishDatetime').datetimepicker({
                                    format: 'd/m/Y',
                                    timepicker: false,
                                    minDate: minDate.toISOString(),
                                    onChangeDateTime: function(e) {
                                        // $scope.finishDatetime = e;
                                        // var dateTxt = $('.finishDatetime').datetimepicker('getValue');
                                        // if(Util.isValueExist(dateTxt)) {
                                        // 	dateTxt = Util.getOnlyDate(new Date(dateTxt));
                                        // 	// $log.info('convert dateTxt=' + dateTxt);
                                        // }

                                        // if(dateTxt !== null && dateTxt !== undefined) {
                                        // 	$scope.finishDatetime = new Date(dateTxt);
                                        // }
                                        // $log.info("finishDatetime=" + $scope.finishDatetime);
                                        // $log.info("finishDatetime=" + $scope.finishDatetime);
                                    }
                                });
                            },
                            preConfirm: function() {}
                        }).then(function(data) { // click play.
                            $scope.isLoadingPopup = false;
                        }, function(dismiss) { // otherwise.
                            $scope.isLoadingPopup = false;
                        }).catch(swal.noop);

                    },
                    function(error) {
                        if (error == "re-signin") {
                            swal.close();
                            obj.signinPopup();
                            throw "session timeout";
                        } else
                            throw error;
                    }
                );

            },
            requestGroupPopup: function(game) {
                swal({
                    title: 'Request Access',
                    html: 'Do you request access to <strong class="text-underline">' + game + '</strong> Game?',
                    showCloseButton: true,
                    showCancelButton: true,
                    focusConfirm: false,
                    confirmButtonText: 'Request',
                    cancelButtonText: 'Close',
                    preConfirm: function() {
                        return Service.requestGroup(game, undefined, obj).then(
                            function(response) {
                                swal.close();
                            },
                            function(error) {
                                if (error == "re-signin") {
                                    swal.close();
                                    obj.signinPopup();
                                    throw "session timeout";
                                } else
                                    throw error;
                            }
                        );
                    }
                }).then(function(data) {
                    swal({
                        position: 'top-center',
                        type: 'success',
                        title: '<h3 class="font-bold">Your request has been sent successfully</h3>',
                        showConfirmButton: false,
                        timer: 5000
                    }).catch(swal.noop)
                    $route.reload();
                }, function(dismiss) {}).catch(swal.noop);
            },
            unexpectErrorPopup: function() {
                swal({
                    title: 'Oops...',
                    text: 'Something went wrong!',
                    type: 'error',
                    showConfirmButton: false,
                    showCancelButton: true,
                    cancelButtonText: 'Close'
                }).then(function(data) {}, function(dismiss) {
                    $window.location.href = '#!/signin';
                });
            },
            successPopup: function(successMsg) {
                swal({
                    type: 'success',
                    title: 'Success',
                    html: successMsg,
                }).then(obj.reload, obj.reload).catch(swal.noop);
            },
            failPopup: function(failMsg, refresh) {
                if (refresh) {
                    swal({
                        type: 'error',
                        title: 'Failure',
                        html: failMsg
                    }).then(function(response) { $window.location.reload(); }, function(dismiss) { $window.location.reload(); }).catch(function(error) {
                        // console.log(error);
                    });
                } else {
                    swal({
                        type: 'error',
                        title: 'Failure',
                        html: failMsg
                    }).then(obj.reload, obj.reload).catch(function(error) {
                        // console.log(error);
                    });
                }
            },
            success: function(response) {
                $route.reload();
            },
            error: function(error) {
                if (error == 're-signin') {
                    obj.signinPopup();
                } else {
                    swal({
                        type: 'error',
                        html: error
                    }).catch(swal.noop);
                }
            },
            convertObjectToArray: function(obj) {
                return typeof obj == 'object' && obj != null ? Object.values(obj) : [];
            },
            isPropExist: function(obj, key) {
                var keys = Object.keys(obj);
                return keys.indexOf(key);
            },
            convertSortName: function(isReverse) { // only smart-table
                return isReverse ? 'desc' : 'asc';
            },
            clearUser: function() {
                $rootScope.user = undefined;
                $localStorage.$reset();
            },
            getInnerHTMLById: function(id) {
                return document.getElementById(id).innerHTML;
            },
            getOnlyDate: function(date) {
                var result = null;
                if (date !== null && date !== undefined) {
                    result = ":m/:d/:y"
                        .replace(":d", date.getDate())
                        .replace(":m", date.getMonth() + 1)
                        .replace(":y", date.getFullYear());
                }
                return result;
            },
            isValueExist: function(value) {
                return value !== null && value !== undefined;
            },
            filterField: function(field) {
                switch (field) {
                    case 'group_name':
                        return 'grp_name';
                    case 'created_by':
                        return 'grp_createdby';
                    case 'players':
                        return 'players';
                    default:
                        return 'grp_dttm';
                }
            },
            reload: function() {
                $route.reload();
            },
            getTimezone: function() {
                var offset = new Date().getTimezoneOffset(),
                    o = Math.abs(offset);
                return (offset < 0 ? "+" : "-") + ("00" + Math.floor(o / 60)).slice(-2) + ":" + ("00" + (o % 60)).slice(-2);
            }
        };
        return obj;
    }
]);