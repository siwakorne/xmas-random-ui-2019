'use strict';

var app = angular.module('ChristmasRandom');

app.directive('pageSelect', function() {
  return {
    restrict: 'E',
    templateUrl: 'resources/template/input.directive.html',
    link: function(scope, element, attrs) {
      scope.$watch('currentPage', function(c) {
        scope.inputPage = c;
      });
    }
  }
});