'use strict';

var app = angular.module('ChristmasRandom');

app.factory('Auth', ['$http', '$q', '$log', 'uuid2', 'User', function ($http, $q, $log, uuid2, User) {
    return {
        getUser: function () {
            return User;
        }
    }
}]);