'use strict';

var app = angular.module('ChristmasRandom');

app.directive('test', function () {
    return {
        restrict: 'E',
        // templateUrl: 'resources/template/input.directive.html',
        controller: function ($scope, $rootScope) {
            // $scope.data = $rootScope.roomName;
            console.log("inside controller...");
        },
        template: '<button type="button">{{data}} - test</button>',
        scope: { data: '@' },
        link: function (scope, element, attrs, rootScope) {
            // scope.data = "test-data in directive."
            // scope.data = rootScope.roomName;
            console.log("inside directive");
            console.log(scope.data);
        }
    }
});